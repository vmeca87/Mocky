FROM hseeberger/scala-sbt 
MAINTAINER Victor Cabello <vmeca87@gmail.com> 

COPY . /app

WORKDIR /app
RUN sbt -Dsbt.override.build.repos=true -Dsbt.repository.secure=false dist

CMD ["sbt", "-Dsbt.override.build.repos=true", "-Dsbt.repository.secure=false", "run", "."]